/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"
#include <stdio.h>

#define ESPERA_SALIDA_SEG   5

#define BARRERA_BAJA        0
#define INGRESANDO          1
#define ESPERANDO_SALIDA    2  
#define ALARMA              3
/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/



/*==================[end of file]============================================*/
int main(void)
{
    uint8_t input;
    uint8_t timer;
    uint8_t state;
    hw_Init();
    state = 0, timer = 0;                                          //podria ir en inicializacion
   
    while (input != EXIT) {
        printf("Estado actual: %d\n",state);
        input = hw_LeerEntrada();

        switch(state){  
            case BARRERA_BAJA:
                if (input == SENSOR_2){
                    hw_SonarAlarma();
                    state = ALARMA;
                }
                else if(input == SENSOR_1){
                    hw_AbrirBarrera();
                    state = INGRESANDO;
                }
                break;

            case INGRESANDO:
                if(input == SENSOR_2){
                    timer=0;
                    state = ESPERANDO_SALIDA;
                }
                break;
            
            case ESPERANDO_SALIDA:
                if(input == SENSOR_1){
                    state = INGRESANDO;
                }
                else if(timer< ESPERA_SALIDA_SEG*1000){ 
                    hw_Pausems(1);
                    timer++;
                }
                else if(timer == ESPERA_SALIDA_SEG*1000){
                    hw_CerrarBarrera();
                    state = BARRERA_BAJA;
                }
                break;

            case ALARMA:
                if(input != SENSOR_2){                   
                    hw_ApagarAlarma();
                    state = BARRERA_BAJA;
                }
                break;
        }
        hw_Pausems(100);
   }

    hw_DeInit();
    return 0;
}

