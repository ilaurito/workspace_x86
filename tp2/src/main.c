/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"
#include <stdio.h>
/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input=1;
    uint8_t state = REPOSO;
    uint8_t timer;
    _Bool led = 1;
    hw_Init();

    while (input != EXIT) {
        input = hw_LeerEntrada();
        //printf("\n Estado actual: %d", state);
        switch (state){
            case REPOSO:
                if(input == INGRESO_FICHA){
                    timer = 0;
                    state = SELECCION;
                }
                hw_Pausems(1000);
                toggleLED(led);
                /*if(ev_Tick1seg_raised){
                    toggleLED();   Para cuando tenga el hardware
                }*/
                break;
            
            case SELECCION:
                if (input == BOTON_TE){
                    timer = 0;
                    prepararTe();
                    state = TE;
                }
                else if (input == BOTON_CAFE){
                    timer = 0;
                    prepararCafe();
                    state = CAFE;
                }
                else if(timer < 30){
                    hw_Pausems(1000);
                    printf("\nEsperando ficha");
                    timer++;
                }
                else if(timer == 30){
                    devolucionFicha();
                    state = REPOSO;
                }
                /*else if(ev_Tick1seg_raised && timer < 30){
                    timer++;
                 }
                else if(ev_Tick1seg_raised && timer == 30){
                    devolucionFicha();
                    state = REPOSO;*/
                break;
            
            case TE:
                if(timer < (TIEMPO_TE * FRECUENCIA_PARPADEO)){
                    hw_Pausems(200);
                    toggleLED(led);
                    timer++;
                }
                else if(timer == (TIEMPO_TE * FRECUENCIA_PARPADEO)){
                    bebidaTerminada();
                    timer=0;
                    state = ALARMA;
                }
                /*if(ev_Tick200ms_raised && timer < TIEMPO_TE * FRECUENCIA_PARPADEO){
                    toggleLED();
                    timer++;
                }
                else if(ev_Tick200ms_raised && timer == TIEMPO_TE * FRECUENCIA_PARPADEO){
                    bebidaTerminada();
                    timer=0;
                    state = ALARMA;
                    }*/
                break;
            
            case CAFE:
                if(timer < (TIEMPO_CAFE * FRECUENCIA_PARPADEO)){
                    hw_Pausems(200);
                    toggleLED(led);
                    timer++;    
                }
                else if(timer == (TIEMPO_CAFE * FRECUENCIA_PARPADEO)){
                    bebidaTerminada();
                    timer=0;
                    state = ALARMA;
                }
                /*if(ev_Tick200ms_raised && timer < TIEMPO_CAFE * FRECUENCIA_PARPADEO){
                    toggleLED();
                    timer++;
                }
                else if(ev_Tick200ms_raised && timer == TIEMPO_CAFE * FRECUENCIA_PARPADEO){
                    bebidaTerminada();
                    timer=0;
                    state = ALARMA;
                }*/
                break;

            case ALARMA:
                indicadorSonoro();
                if(timer < 2){
                    hw_Pausems(1000);
                    timer++;
                }
                else if(timer == 2){
                    apagarSonido();
                    state = REPOSO;
                }
                /*if(ev_Tick1seg_raised && timer < 2){
                    timer++;
                }
                else if(ev_Tick1seg_raised && timer == 2){
                    apagarSonido();
                    state = REPOSO;
                }*/
                break;
            }
        hw_Pausems(100);
        }
    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
