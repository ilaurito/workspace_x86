/*==================[inclusions]=============================================*/

#include "hw.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);       
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);    
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}

void toggleLED(_Bool led){
    led = !led;
    printf("%d\n", led);
    //printf("\nLed: %s ", led ? "true" : "false");
}

void prepararCafe(){
    printf("\nPreparando Cafe");
}
void prepararTe(){
    printf("\nPreparando te");
}
void devolucionFicha(){
    printf("\n Devolviendo Ficha");
}

void bebidaTerminada(){
    printf("\nBebida terminada");
}

void indicadorSonoro(){
    printf("\nSonido de alarma");
}

void apagarSonido(){
    printf("\nApagando alarma");
}
/*==================[end of file]============================================*/
