#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT                27  // ASCII para la tecla Esc 
#define BOTON_TE            49  // ASCII para la tecla 1 
#define BOTON_CAFE          50  // ASCII para la tecla 2 
#define INGRESO_FICHA       51
#define ESPERA_TIEMPO_SELECCION 30
#define TIEMPO_CAFE             45
#define TIEMPO_TE               30
#define FRECUENCIA_PARPADEO     5
/*==================[typedef]================================================*/

typedef enum {
    REPOSO,
    SELECCION,
    TE,
    CAFE,
    ALARMA
} CAFETERA_STATES;
/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

void hw_Init(void);
void hw_DeInit(void);
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);
void toggleLED(_Bool led);
void prepararCafe(void);
void prepararTe(void);
void devolucionFicha(void);
void bebidaTerminada(void);
void indicadorSonoro(void);
void apagarSonido(void);



/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
