/*==================[inclusions]=============================================*/

#include "cafetera.h"
#include "hw.h"
#include <stdio.h>
#include <stdbool.h>

/*==================[macros and definitions]=================================*/

#define TIEMPO_TE  30
#define TIEMPO_CAFE 45
#define FRECUENCIA_PARPADEO 5
/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

CAFETERA_STATES state;

bool evSelecCafe;
bool evSelecTe;
bool evIngresaFicha;
bool evTick100ms_raised;
bool evTick500ms_raised;
bool evTick1seg_raised;
uint8_t timer=0;
bool led=0;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void clearEvents(void)
{
    evSelecCafe = 0;
    evSelecTe = 0;
    evIngresaFicha = 0;
    evTick100ms_raised = 0;
    evTick1seg_raised = 0;
}

/*==================[external functions definition]==========================*/

void cafetera_init(void)
{
    state = REPOSO;
    clearEvents();
}

void cafetera_runCycle(void)
{
    switch (state){
            case REPOSO:
                if(evIngresaFicha){
                    timer = 0;
                    state = SELECCION;
                }
                else if(evTick500ms_raised){
                        toggleLED(led);
                        }
                break;
            
            case SELECCION:
                if (evSelecTe){
                    timer = 0;
                    prepararTe();
                    state = TE;
                }
                else if (evSelecCafe){
                    timer = 0;
                    prepararCafe();
                    state = CAFE;
                }
                else if(evTick1seg_raised && timer < 30){
                    printf("\nEsperando ficha\n");
                    timer++;
                 }
                else if(evTick1seg_raised && timer == 30){
                    devolucionFicha();
                    state = REPOSO;
                }
                break;
            
            case TE:
                if(evTick100ms_raised && timer < TIEMPO_TE * FRECUENCIA_PARPADEO){
                    toggleLED(led);
                    timer++;
                }
                else if(evTick100ms_raised && timer == TIEMPO_TE * FRECUENCIA_PARPADEO){
                    bebidaTerminada();
                    timer=0;
                    state = ALARMA;
                    }
                break;
            
            case CAFE:
                if(evTick100ms_raised && timer < TIEMPO_CAFE * FRECUENCIA_PARPADEO){
                    toggleLED(led);
                    timer++;
                }
                else if(evTick100ms_raised && timer == TIEMPO_CAFE * FRECUENCIA_PARPADEO){
                    bebidaTerminada();
                    timer=0;
                    state = ALARMA;
                }
                break;

            case ALARMA:
                indicadorSonoro();
                if(evTick1seg_raised && timer < 2){
                    timer++;
                }
                else if(evTick1seg_raised && timer == 2){
                    apagarSonido();
                    state = REPOSO;
                }
                break;
            }
    clearEvents();
}
    

void cafetera_raise_evSelecCafe(void)
{
    evSelecCafe = 1;
}

void cafetera_raise_evSelecTe(void)
{
    evSelecTe = 1;
}

void cafetera_raise_evIngresaFicha(void)
{
    evIngresaFicha = 1;
}

void cafetera_raise_evTick200ms(void)
{
    evTick100ms_raised = 1;
}

void cafetera_raise_evTick500ms(void)
{
    evTick100ms_raised = 1;
}

void cafetera_raise_evTick1seg(void)
{
    evTick1seg_raised = 1;
}

void cafetera_printCurrentState(void)
{
    printf("Estado actual: %0d \n", state);
}

void toggleLED(bool led){
    led = !led;
    printf("%d\n", led);
    //printf("\nLed: %s ", led ? "true" : "false");
}

void prepararCafe(){
    printf("\nPreparando Cafe");
}
void prepararTe(){
    printf("\nPreparando te");
}
void devolucionFicha(){
    printf("\n Devolviendo Ficha");
}

void bebidaTerminada(){
    printf("\nBebida terminada");
}

void indicadorSonoro(){
    printf("\nSonido de alarma");
}

void apagarSonido(){
    printf("\nApagando alarma");
}
/*==================[end of file]============================================*/
