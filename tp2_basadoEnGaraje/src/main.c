/*==================[inclusions]=============================================*/

#include "main.h"
#include "cafetera.h"
#include "hw.h"
#include <stdio.h>
/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input = 0;
    uint16_t cont_1s = 0;
    uint16_t cont_100ms = 0;
    uint16_t cont_500ms = 0;
    hw_Init();

    cafetera_init();
    printf("\n Para ingresar ficha presione 3");
    printf("\n Para elegir bebida:\n 1 : CAFE\n 2 : TE\n");

    while (input != EXIT) {
        input = hw_LeerEntrada();

        if (input == BOTON_CAFE) {
            cafetera_raise_evSelecCafe();
        }

        if (input == BOTON_TE) {
            cafetera_raise_evSelecTe();
        }
        if (input == INGRESO_FICHA) {
            cafetera_raise_evIngresaFicha();
        }
        cont_1s++;
        cont_100ms++;
        cont_500ms++;
        if (cont_1s == 1000) {
            cont_1s = 0;
            cafetera_raise_evTick1seg();
        }
        if (cont_100ms == 100) {
            cont_100ms = 0;
            cafetera_raise_evTick200ms();
        }
        if (cont_500ms == 100) {
            cont_500ms = 0;
            cafetera_raise_evTick500ms();
        }

        cafetera_runCycle();
        hw_Pausems(1);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
