#ifndef _CAFETERA_H_
#define _CAFETERA_H_

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

typedef enum {
    REPOSO,
    SELECCION,
    TE,
    CAFE,
    ALARMA
} CAFETERA_STATES;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Inicializacion y evaluacion de la FSM
void cafetera_init(void);
void cafetera_runCycle(void);

// Eventos
void cafetera_raise_evSelecCafe(void);
void cafetera_raise_evSelecTe(void);
void cafetera_raise_evIngresaFicha(void);
void cafetera_raise_evTick100ms(void);
void cafetera_raise_evTick500ms(void);
void cafetera_raise_evTick1seg(void);
// Debugging
void cafetera_printCurrentState(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _CAFETERA_H_ */
