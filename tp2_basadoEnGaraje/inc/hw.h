#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>
#include <stdbool.h>
/*==================[macros]=================================================*/

#define EXIT            27  // ASCII para la tecla Esc
#define BOTON_CAFE      49  // ASCII para la tecla 1
#define BOTON_TE        50  // ASCII para la tecla 2
#define INGRESO_FICHA   51
/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Funciones que configuran la consola de Linux como interfaz de I/O
void hw_Init(void);
void hw_DeInit(void);

// Funciones basicas para leer que entrada esta activa y pausar la ejecucion
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);
void toggleLED(bool led);
void prepararCafe(void);
void prepararTe(void);
void devolucionFicha(void);
void bebidaTerminada(void);
void indicadorSonoro(void);
void apagarSonido(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
